#!/usr/bin/env bash

# download
cd ~ && wget -O - "https://www.dropbox.com/download?plat=lnx.x86_64" | tar xzf -

# start deamon
~/.dropbox-dist/dropboxd &
