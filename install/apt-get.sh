#!/usr/bin/env bash

# Globally add packages
packages=(
	ppa:linrunner/tlp
	ppa:duggan/bats
	ppa:gekkio/xmonad
)


# Globally install programs
apt=(
	bats
	cmake
	cpp
	gcc
	git 
	nautilus-dropbox
	rxvt-unicode
	snapd
	thunderbird
	tlp
	tlp-rdw
	tmux
	tree
	vim
	wine64
	wget
)

for i in "${packages[@]}"; do
	sudo add-apt-repository $i -y 
done
apt-get update -y
apt-get upgrade -y
apt-get install "${apt[@]}" -y
