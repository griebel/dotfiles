#!/usr/bin/env bash

# Globally install programs
snap=(
	"atom --classic"
	"clion --classic"
	"gimp"
	"inkscape"
	"lepton"
	"pdfmixtool"
	"pycharm-professional --classic"
	"spotify"
	"vlc"
)

snap refresh
for i in "${snap[@]}"; do
	snap install $i
done
