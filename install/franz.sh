#!/usr/bin/env bash

TEMP_DEB="$(mktemp)" &&
wget -O "$TEMP_DEB" 'https://github.com/meetfranz/franz/releases/download/v5.0.0-beta.18/franz_5.0.0-beta.18_amd64.deb' &&
sudo dpkg -i "$TEMP_DEB"
rm -f "$TEMP_DEB"

apt-get --fix-broken install -y
